[![GPL licensed](https://img.shields.io/badge/license-GPL%20v2-blue.svg)]()
[![GitHub tag](https://img.shields.io/github/tag/evemondevteam/evemon.svg)]()

[![Open Hub](https://www.openhub.net/p/evemon/widgets/project_thin_badge.gif)](https://www.openhub.net/p/evemon)

[![Twitter Follow](https://img.shields.io/twitter/follow/EVEMon.svg?style=social)](https://twitter.com/evemon)

# **EVEMon** 

A lightweight, easy-to-use standalone Windows application designed to assist you in keeping track of your EVE Online character progression.

For complete info on *How To Contribute* please visit the [wiki](https://bitbucket.org/EVEMonDevTeam/evemon/wiki) section

**Owner:** 

[EVEMonDevTeam](https://github.com/evemondevteam/) (GitHub)


**Website:** [EVEMon](https://evemondevteam.github.io/evemon/)